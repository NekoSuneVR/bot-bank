package com.eclipsekingdom.botbank.spigot.plugin;

import com.eclipsekingdom.botbank.spigot.SpigotMain;
import com.eclipsekingdom.botbank.util.plugin.ConfigLoader;
import com.eclipsekingdom.botbank.util.plugin.IConsole;
import com.eclipsekingdom.botbank.util.plugin.IPlugin;
import com.eclipsekingdom.botbank.util.plugin.IScheduler;
import org.bukkit.Bukkit;

public class SpigotPlugin implements IPlugin {

    private SpigotScheduler scheduler = new SpigotScheduler();
    private String version = SpigotMain.getPlugin().getDescription().getVersion();
    private SpigotConfigLoader configLoader = new SpigotConfigLoader();
    private IConsole console = new SpigotConsole();

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public ConfigLoader getConfigLoader() {
        return configLoader;
    }

    @Override
    public IScheduler getScheduler() {
        return scheduler;
    }

    @Override
    public IConsole getConsole() {
        return console;
    }

    @Override
    public int getOnlinePlayerCount() {
        return Bukkit.getOnlinePlayers().size();
    }

}
