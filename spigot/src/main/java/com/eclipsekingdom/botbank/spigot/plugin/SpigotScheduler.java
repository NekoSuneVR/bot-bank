package com.eclipsekingdom.botbank.spigot.plugin;

import com.eclipsekingdom.botbank.spigot.SpigotMain;
import com.eclipsekingdom.botbank.util.plugin.IScheduler;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class SpigotScheduler implements IScheduler {

    private BukkitScheduler scheduler = Bukkit.getScheduler();
    private Plugin plugin = SpigotMain.getPlugin();

    @Override
    public void runAsync(Runnable r) {
        scheduler.runTaskAsynchronously(plugin, r);
    }

}
