package com.eclipsekingdom.botbank.spigot.plugin;

import com.eclipsekingdom.botbank.util.plugin.ConfigLoader;
import com.eclipsekingdom.botbank.util.plugin.FileConfig;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class SpigotConfigLoader implements ConfigLoader {

    @Override
    public FileConfig load(File file) {
        return new SpigotConfig(YamlConfiguration.loadConfiguration(file));
    }

}
