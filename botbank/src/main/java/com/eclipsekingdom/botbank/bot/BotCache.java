package com.eclipsekingdom.botbank.bot;

import com.eclipsekingdom.botbank.config.BotsConfig;
import com.eclipsekingdom.botbank.command.IBotCommandExecutor;
import com.eclipsekingdom.botbank.command.CommandListener;

import java.util.*;

public class BotCache {

    private static List<Bot> bots = new ArrayList<>();
    private static Map<String, Bot> nameToBot = new HashMap<>();
    private static Map<UUID, Map<String, IBotCommandExecutor>> botToCommands = new HashMap<>();
    private static Map<UUID, CommandListener> botToListener = new HashMap<>();

    public static void load() {
        bots.addAll(BotsConfig.getBots());
        for (Bot bot : bots) {
            nameToBot.put(bot.getName(), bot);
        }
    }

    public static void shutdown() {
        for (Bot bot : bots) {
            bot.shutdown();
        }
    }

    public static Bot getBot(String name) {
        return nameToBot.get(name);
    }

    public static List<Bot> getBots() {
        return bots;
    }

    public static void registerBotCommand(Bot bot, String namespace, String root, IBotCommandExecutor orderExecutor) {
        UUID botId = bot.getId();
        String effectiveRoot = clean(root);
        if (botToCommands.containsKey(botId)) {
            Map<String, IBotCommandExecutor> rootToExecutor = botToCommands.get(botId);
            if (rootToExecutor.containsKey(effectiveRoot)) {
                rootToExecutor.put(namespace + ":" + effectiveRoot, orderExecutor);
            } else {
                rootToExecutor.put(effectiveRoot, orderExecutor);
            }
        } else {
            Map<String, IBotCommandExecutor> rootToExecutor = new HashMap<>();
            rootToExecutor.put(effectiveRoot, orderExecutor);
            botToCommands.put(botId, rootToExecutor);
            //register listener
            if (botToListener.containsKey(botId)) {
                botToListener.get(botId).register();
            } else {
                CommandListener commandListener = new CommandListener(bot);
                commandListener.register();
                botToListener.put(botId, commandListener);
            }
        }
    }

    private static String clean(String s) {
        return s.replaceAll(":", "").replaceAll(" ", "").toLowerCase();
    }

    public static void unregisterBotCommand(UUID botId, String namespace, String root) {
        if (botToCommands.containsKey(botId)) {
            String effectiveRoot = clean(root);
            Map<String, IBotCommandExecutor> rootToExecutor = botToCommands.get(botId);
            if (rootToExecutor.containsKey(namespace + ":" + effectiveRoot)) {
                rootToExecutor.remove(namespace + ":" + effectiveRoot);
            } else {
                rootToExecutor.remove(namespace);
            }
            //unregister listener
            if (rootToExecutor.isEmpty()) {
                if (botToListener.containsKey(botId)) {
                    botToListener.get(botId).unregister();
                }
            }
        }
    }

    public static IBotCommandExecutor getExecutor(UUID botId, String root) {
        if (botToCommands.containsKey(botId)) {
            Map<String, IBotCommandExecutor> rootToExecutor = botToCommands.get(botId);
            return rootToExecutor.get(root.toLowerCase());
        } else {
            return null;
        }
    }


}
