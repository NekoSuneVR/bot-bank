package com.eclipsekingdom.botbank.util.plugin;

import java.util.Collection;
import java.util.List;

public interface FileConfig {

    boolean contains(String path);

    Collection<String> getSection(String path);

    String getString(String path, String defaultValue);

    int getInt(String path, int defaultValue);

    boolean getBoolean(String path, boolean defaultValue);

}
