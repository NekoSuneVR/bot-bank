package com.eclipsekingdom.botbank.util.plugin;

public class Console {

    private static IConsole console;

    public static void init(IConsole toSet) {
        console = toSet;
    }

    public static void raw(String s) {
        console.raw(s);
    }

    public static void info(String s) {
        console.info(s);
    }

    public static void warn(String s) {
        console.warn(s);
    }

}
