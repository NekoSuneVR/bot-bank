package com.eclipsekingdom.botbank.server;

import com.eclipsekingdom.botbank.bot.Bot;
import com.eclipsekingdom.botbank.bot.BotCache;
import com.eclipsekingdom.botbank.bot.BotStatus;
import com.eclipsekingdom.botbank.util.BulkTask;
import com.eclipsekingdom.botbank.util.plugin.Console;

public class PresenceUpdater extends BulkTask {

    public PresenceUpdater() {
        super(3, 6);
    }

    @Override
    protected void runTask() {
        for (Bot bot : BotCache.getBots()) {
            if (bot.getStatus() == BotStatus.ONLINE && bot.getPresence().hasOnlinePlaceholder()) {
                bot.updatePresence();
            }
        }
    }

}
