package com.eclipsekingdom.botbank.server;

import com.eclipsekingdom.botbank.util.plugin.Plugin;

public class Server {

    private static PresenceUpdater presenceUpdater;
    private static int onlinePlayers;

    public static void load() {
        onlinePlayers = Plugin.getOnlinePlayerCount();
        presenceUpdater = new PresenceUpdater();
    }

    public static void onJoin() {
        onlinePlayers++;
        presenceUpdater.onExtend();
    }

    public static void onQuit() {
        onlinePlayers--;
        presenceUpdater.onExtend();
    }

    public static int getOnlinePlayers() {
        return onlinePlayers;
    }

}
