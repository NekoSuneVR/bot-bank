package com.eclipsekingdom.botbank;

import com.eclipsekingdom.botbank.bot.BotCache;
import com.eclipsekingdom.botbank.config.BotsConfig;
import com.eclipsekingdom.botbank.config.ConfigLoader;
import com.eclipsekingdom.botbank.config.PluginConfig;
import com.eclipsekingdom.botbank.server.Server;
import com.eclipsekingdom.botbank.util.plugin.Console;
import com.eclipsekingdom.botbank.util.plugin.IPlugin;
import com.eclipsekingdom.botbank.util.plugin.Plugin;

import java.io.InputStream;

public final class BotBank {

    public static void startup(IPlugin iPlugin) {
        Plugin.init(iPlugin);
        Console.init(iPlugin.getConsole());
        ConfigLoader.load();
        PluginConfig.load();
        BotsConfig.load();
        BotCache.load();
        Server.load();
    }

    public static void shutdown() {
        BotCache.shutdown();
    }


    public static InputStream getResource(String path) {
        return BotBank.class.getResourceAsStream(path);
    }

}
