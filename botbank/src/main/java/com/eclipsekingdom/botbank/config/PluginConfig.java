package com.eclipsekingdom.botbank.config;

import com.eclipsekingdom.botbank.util.plugin.FileConfig;
import com.eclipsekingdom.botbank.util.plugin.Plugin;

import java.io.File;

public class PluginConfig {

    private static final String BOT_CONNECTION_TIMEOUT_FIELD = "bot connection timeout";
    private static int botConnectionTimeout = 10;

    private static final String APP_REGISTRATION_PERIOD_FIELD = "app registration period field";
    private static int appRegistrationPeriod = 3;

    public static void load() {
        File file = new File("plugins/BotBank", "bots.yml");
        if (file.exists()) {
            FileConfig config = Plugin.getConfigLoader().load(file);
            try {
                botConnectionTimeout = config.getInt(BOT_CONNECTION_TIMEOUT_FIELD, botConnectionTimeout);
                appRegistrationPeriod = config.getInt(APP_REGISTRATION_PERIOD_FIELD, appRegistrationPeriod);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static int getBotConnectionTimeout() {
        return botConnectionTimeout;
    }

    public static int getAppRegistrationPeriod() {
        return appRegistrationPeriod;
    }

}
