package com.eclipsekingdom.botbank.api;

import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import java.util.Collection;
import java.util.Objects;

public class DiscordApp {

    private String namespace;
    private String botName;
    private Collection<GatewayIntent> gatewayIntents;
    private Collection<CacheFlag> cacheFlags;
    private MemberCachePolicy memberCachePolicy;

    public DiscordApp(String namespace, String botName, Collection<GatewayIntent> gatewayIntents, Collection<CacheFlag> cacheFlags, MemberCachePolicy memberCachePolicy) {
        this.namespace = namespace;
        this.botName = botName;
        this.gatewayIntents = gatewayIntents;
        this.cacheFlags = cacheFlags;
        this.memberCachePolicy = memberCachePolicy;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getBotName() {
        return botName;
    }

    public Collection<GatewayIntent> getGatewayIntents() {
        return gatewayIntents;
    }

    public Collection<CacheFlag> getCacheFlags() {
        return cacheFlags;
    }

    public MemberCachePolicy getMemberCachePolicy() {
        return memberCachePolicy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscordApp that = (DiscordApp) o;
        return Objects.equals(namespace, that.namespace) &&
                Objects.equals(botName, that.botName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(namespace, botName);
    }
}
